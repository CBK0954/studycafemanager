package com.choibk.studycafemanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.event.SpringApplicationEvent;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class CafeUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String userName;

    @Column(nullable = false, length = 20)
    private String userPhone;

    @Column(nullable = false)
    private Integer useTime; // 이용시간

    @Column(nullable = false)
    private LocalDateTime enterTime; // 입실시간

    @Column(nullable = false)
    private LocalDateTime limitTime; // 제한퇴실시간

    private LocalDateTime exitOrNot; // 퇴실여부
}