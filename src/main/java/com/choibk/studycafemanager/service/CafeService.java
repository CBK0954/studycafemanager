package com.choibk.studycafemanager.service;

import com.choibk.studycafemanager.entity.CafeUser;
import com.choibk.studycafemanager.model.UserItem;
import com.choibk.studycafemanager.model.UserRequest;
import com.choibk.studycafemanager.model.UserUseTimeUpdateRequest;
import com.choibk.studycafemanager.repository.CafeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CafeService {
    private final CafeRepository cafeRepository;

    public void setUser(UserRequest request) {
        CafeUser addData = new CafeUser();
        addData.setUserName(request.getUserName());
        addData.setUserPhone(request.getUserPhone());
        addData.setUseTime(request.getUseTime());
        addData.setEnterTime(LocalDateTime.now());
        addData.setLimitTime(LocalDateTime.now().plusHours(request.getUseTime()));

        cafeRepository.save(addData);
    }

    public List<UserItem> getUsers() {
        List<CafeUser> originList = cafeRepository.findAll();

        List<UserItem> result = new LinkedList<>();

        for(CafeUser item : originList) {
            UserItem addItem = new UserItem();
            addItem.setId(item.getId());
            addItem.setUserInfo(item.getUserName() + " / " + item.getUserPhone());
            addItem.setUseTime(item.getUseTime());
            addItem.setUseTimeFullText(item.getEnterTime() + " ~ " + item.getLimitTime());
            addItem.setCheckOut(item.getExitOrNot() == null ? false : true); // 퇴실이 0 (안됬다면) 퇴실안함 1은 반대로
            addItem.setExitOrNot(item.getExitOrNot());

            result.add(addItem);
        }

        return result;
    }

    public void putUserUseTime(long id, UserUseTimeUpdateRequest request) {
        CafeUser originData = cafeRepository.findById(id).orElseThrow();
        originData.setUseTime(request.getUseTime());
        originData.setLimitTime(originData.getEnterTime().plusHours(request.getUseTime()));

        cafeRepository.save(originData);
    }

    public void putUserCheckOut(long id) {
        CafeUser originData = cafeRepository.findById(id).orElseThrow();
        originData.setExitOrNot(LocalDateTime.now());

        cafeRepository.save(originData);
    }

    public void delUser(long id) {
        cafeRepository.deleteById(id);
    }
}
