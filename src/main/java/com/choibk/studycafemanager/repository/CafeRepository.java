package com.choibk.studycafemanager.repository;

import com.choibk.studycafemanager.entity.CafeUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CafeRepository extends JpaRepository<CafeUser, Long> {
}
