package com.choibk.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class UserItem {
    private long id;
    private String userInfo;
    private Integer useTime;
    private String useTimeFullText; // 입실 시간 ~ 퇴실 시간 표시
    private Boolean checkOut; // 퇴실 시간
    private LocalDateTime exitOrNot; //
}
