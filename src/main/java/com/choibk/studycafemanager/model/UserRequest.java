package com.choibk.studycafemanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String userName;

    @NotNull
    @Length(min = 11, max = 20)
    private String userPhone;

    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer useTime;
}