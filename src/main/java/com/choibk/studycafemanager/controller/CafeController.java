package com.choibk.studycafemanager.controller;

import com.choibk.studycafemanager.model.UserItem;
import com.choibk.studycafemanager.model.UserRequest;
import com.choibk.studycafemanager.model.UserUseTimeUpdateRequest;
import com.choibk.studycafemanager.service.CafeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "스터디 카페 회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user")
public class CafeController {
    private final CafeService cafeService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/data")
    public String setUser(@RequestBody @Valid UserRequest request) {
        cafeService.setUser(request);

        return "Done";
    }

    @ApiOperation(value = "회원 전체 조회")
    @GetMapping("/all")
    public List<UserItem> getUsers() {
        List<UserItem> result = cafeService.getUsers();

        return result;
    }

    @ApiOperation(value = "회원 이용 시간")
    @PutMapping("/use-time/id/{id}")
    public String putUserUseTime(@PathVariable long id, @RequestBody @Valid UserUseTimeUpdateRequest request) {
        cafeService.putUserUseTime(id, request);

        return "Done";
    }

    @ApiOperation(value = "회원 퇴실 (수정)")
    @PutMapping("/check-out/id/{id}")
    public String putUserCheckOut(@PathVariable long id) {
        cafeService.putUserCheckOut(id);

        return "Done";
    }

    @ApiOperation(value = "회원 정보 삭제")
    @DeleteMapping("/sign-out/id/{id}")
    public String delUser(@PathVariable long id) {
        cafeService.delUser(id);

        return "Done";
    }
}